import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";

/* In-Memory Web API */
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryTasksWebService } from "app/shared/tasks/in-memory-tasks-web-service";

/* Application modules */
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { FeatureModule } from './feature/feature.module';
import { FeatureRoutingModule } from './feature/feature-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryTasksWebService),
    AppRoutingModule,
    CoreModule,
    FeatureModule,
    SharedModule,
    FeatureRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
