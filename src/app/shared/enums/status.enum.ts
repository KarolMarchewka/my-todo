export enum Status {
    NotDone,
    InProgress,
    Done
}
