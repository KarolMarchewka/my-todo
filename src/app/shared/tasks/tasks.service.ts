import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, XHRBackend } from "@angular/http";

/* RxJS */
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/* Enums */
import { Status } from '../enums/status.enum';
import { Priority } from '../enums/priority.enum';

/* Models */
import { Task } from "app/shared/models/task";

@Injectable()
export class TasksService {
  constructor(private http: Http) {}

  getTasks(): Observable<Task[]> {
    let tasksUrl = 'api/tasks'; 

    return this.http.get(tasksUrl)
      .map((res: Response) => res.json().data)
      .catch((error: any) => Observable.throw(error.json().error || "Server error"));
  }

  putTask(task: Task): Observable<Response> {
    let tasksUrl = 'api/tasks'; 
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
      .put(tasksUrl, JSON.stringify(task), { headers: headers });
  }
}
