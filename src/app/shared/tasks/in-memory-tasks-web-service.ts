import { InMemoryDbService } from 'angular-in-memory-web-api';

/* Enums */
import { Status } from "app/shared/enums/status.enum";
import { Priority } from "app/shared/enums/priority.enum";

export class InMemoryTasksWebService implements InMemoryDbService {
    createDb() {
        let tasks = [
            {
                title: 'Buy a car',
                description: "I'd like to buy new car",
                status: Status.Done,
                priority: Priority.Heigh
            },
            {
                title: 'Angular 4.3',
                description: 'Get familiar with the article about new features in the Angular 4.3 version.',
                status: Status.Done,
                priority: Priority.Heigh
            },
            {
                title: 'Shopping',
                description: 'Buy sth for a dinner',
                status: Status.NotDone,
                priority: Priority.Low
            },
            {
                title: 'TODO app.',
                description: 'Create the TODO app as a result of the Anuglar training with Andrzej',
                status: Status.InProgress,
                priority: Priority.Heigh
            },
            {
                title: 'Internet',
                description: 'Payment for internet from ENEA',
                status: Status.NotDone,
                priority: Priority.Heigh
            }
        ];

        return {tasks};
    }
}
