import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
/* Services */
import { TasksService } from './tasks/tasks.service';

/* Pipes */
import { TaskFilterPipe } from './pipes/task-filter/task-filter.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [TaskFilterPipe],
  exports: [
    CommonModule,
    FormsModule,
    TaskFilterPipe
  ],
  providers: [TasksService]
})
export class SharedModule { }
