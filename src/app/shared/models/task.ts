/* Enums */
import { Status } from "app/shared/enums/status.enum";
import { Priority } from "app/shared/enums/priority.enum";

export interface Task {
    title: string;
    description: string;
    priority: Priority;
    status: Status;
}
