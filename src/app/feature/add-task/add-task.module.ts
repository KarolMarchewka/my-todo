import { NgModule } from '@angular/core';

/* Modules */
import { SharedModule } from './../../shared/shared.module';

/* Routing */
import { AddTaskRoutingModule } from './add-task-routing.module';

/* Components */
import { AddTaskComponent } from './add-task.component';

@NgModule({
  imports: [
    AddTaskRoutingModule,
    SharedModule
  ],
  declarations: [AddTaskComponent],
  exports: [AddTaskComponent]
})
export class AddTaskModule { 
  constructor() {
    console.log("AddTaskModule has been loaded.")
  }
}
