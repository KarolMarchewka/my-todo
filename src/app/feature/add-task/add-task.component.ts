import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

/* Service */
import { TasksService } from './../../shared/tasks/tasks.service';

/* Models */
import { Task } from "app/shared/models/task";

@Component({
  selector: 'mytodo-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
  task: Task = <Task>{};
  constructor(private tasksService: TasksService, private router: Router) { }

  ngOnInit() {}

  onAdd() {
    this.tasksService.putTask(this.task);
    this.router.navigate(['/tasks']);
  }

  onCancel() {
    window.history.back();
  }
}
