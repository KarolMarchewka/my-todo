import { NgModule } from '@angular/core';

/* Routing */
import { FeatureRoutingModule } from './feature-routing.module';

/* Components */
import { MenuComponent } from './menu/menu.component';
import { LogoComponent } from './logo/logo.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  imports: [
    FeatureRoutingModule,

  ],
  declarations: [
    MenuComponent,
    LogoComponent,
    PageNotFoundComponent
  ],
  exports: [
    MenuComponent, 
    LogoComponent,
    PageNotFoundComponent,
  ]
})
export class FeatureModule { }
