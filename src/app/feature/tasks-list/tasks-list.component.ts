import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

/* RxJS */
import { Subscription } from 'rxjs/Subscription'

/* Services */
import { TasksService } from "app/shared/tasks/tasks.service";

/* lodash */
import * as _ from 'lodash';

/* Models */
import { Task } from "app/shared/models/task";


@Component({
  selector: 'mytodo-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit {
  tasksList: Task[];
  searchTask: string;
  isTaskSelected = false;

  private instanceSubscriptions = new Subscription();

  constructor(private taskService: TasksService) {}

  ngOnInit() {
    this.instanceSubscriptions
      .add(this.taskService.getTasks()
        .subscribe((tasks: Task[]) => this.tasksList = tasks));
  }

  removeTask(task: Task) {
    _.remove(this.tasksList, (t: Task) => {
      return t.title === task.title;
    });
  }

  editTask(task: Task) {
    this.isTaskSelected = true;
  }
}
