import { NgModule } from '@angular/core';

/* Routing */
import { TasksListRoutingModule } from './tasks-list-routing.module';

/* Modules */
import { SharedModule } from './../../shared/shared.module';

/* Components */
import { TasksListComponent } from './tasks-list.component';

@NgModule({
  imports: [
    TasksListRoutingModule,
    SharedModule
  ],
  declarations: [TasksListComponent],
  exports: [TasksListComponent]
})
export class TasksListModule {
  constructor() {
    console.log("TasksListModule has been loaded.")
  }
 }
