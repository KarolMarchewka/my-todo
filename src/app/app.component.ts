import { Component } from '@angular/core';

@Component({
  selector: 'mytodo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
