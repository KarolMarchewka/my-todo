import { browser, by, element } from 'protractor';

export class MyTodoPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('mytodo-root h1')).getText();
  }
}
